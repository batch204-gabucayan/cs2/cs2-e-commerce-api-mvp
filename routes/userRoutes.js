const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route to User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to User login/Athentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for User createOrder
router.post("/createOrder", (req, res) => {
	const userData ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id

	} 

	if(userData.isAdmin === false) {
		userController.createOrder(req.body, req.userData).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Admin cannot create Orders!");
	}
});

module.exports = router;