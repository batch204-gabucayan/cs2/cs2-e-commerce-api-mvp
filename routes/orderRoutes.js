const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const userController = require("../controllers/userController");
const auth = require('../auth.js');
// const Product = require("../models/Product");


// Route for creating an order/Adding to cart
router.post("/createOrder", auth.verify, (req, res) => {

	let data = {
		productId: req.body.productId,
		quantity: req.body.quantity,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	// console.log(auth.decode(req.headers.authorization).id);

	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
	})
	


module.exports = router;