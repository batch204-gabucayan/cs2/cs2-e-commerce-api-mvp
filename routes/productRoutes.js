const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require('../auth.js');
// const Product = require("../models/Product");

// Route for creating/add a Product
router.post("/add", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// console.log(userData.isAdmin);

	if(userData.isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User is not an Admin.")
	}
});

// Route for Retrieving all Active Products
router.get("/allActive", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for Retrieving single/specific product
router.get("/:productId", (req, res) => {
	// console.log(req.params);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Update Product Information *ADMIN only
router.put("/:productId/update", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.updateProduct(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User is not Admin.")
	}
});

// Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("User is not Admin")
	}
});
module.exports = router;