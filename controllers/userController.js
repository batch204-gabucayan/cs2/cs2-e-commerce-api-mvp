const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller for User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		dateOfBirth: reqBody.dateOfBirth,
		address: reqBody.address,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin,
		password: bcrypt.hashSync(reqBody.password, 10)
	});


	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// User Login/Athentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return "No such email!"
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the password match/result of the code above is true
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return "Email or Password is incorrect."
			}
		}
	});
}

// User createOrder
module.exports.createOrder = async (data, userData) => {
	
	let productData = await Product.findById(data.productId);
	// console.log(productData);

	let userUpdate = await {
		products: [
			{
				productId: data.productId,
				productName: productData.productName,
				quantity: data.quantity,
				price: productData.price,
				subTotalAmount: productData.price*data.quantity
			}
		]
	}

	return User.findByIdAndUpdate(userData.userId, userUpdate).then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}