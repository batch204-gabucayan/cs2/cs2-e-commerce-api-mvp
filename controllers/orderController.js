const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth.js");



// Create an Order/Adding to cart (non-Admin)
module.exports.createOrder = async (data) => {
	if(data.isAdmin === false){

		// console.log(data.userId);

			// Search for Product Data
			let productData = await Product.findById(data.productId)
			// console.log(productData);
			// Create new order
			let newOrder = await new Order ({
					userId: data.userId,
					products: [
					{
						productId: data.productId,
						productName: productData.productName,	
						quantity: data.quantity,
						price: productData.price,
						subTotalAmount: productData.price*data.quantity
					}
					]
				});
			return newOrder.save().then(async (order, error) => {
				if(error) {
					return "Order Failed!"
				} else { /*return order.userId*/
					let userUpdate = await User.findById(order.userId).then(user => { user.orders.push(
						{	
							// orderId: user.aggregate.count("orders")
							products: [
							{
								productId: order.productId,
								productName: order.productName,
								quantity: order.quantity,
								subTotalAmount: order.subTotalAmount
							}
							]
					});
					return user.save().then((user, erro) => {
						if(error){
							return false
						} else{
							return true
						}
					})
				})
				}
			});



	} else {
		return "Admin cannot create an Order"
	}
}
