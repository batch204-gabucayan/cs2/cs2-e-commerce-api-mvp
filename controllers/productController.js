const Product = require("../models/Product");
const bcrypt = require("bcrypt");
// const auth = require("../auth");

// Create a new Product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product ({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {
		if(error) {
			return "Adding Product Failed!"
		} else {
			return "Successfully added new Product!"
		}
	})
}

// Retrieving all Active Products
module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving single/specific Product
module.exports.getProduct = (reqParams) => {
	// console.log(reqParams);
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

// Updating Product information *ADMIN only
module.exports.updateProduct = (reqParams, reqBody, data) => {
		let updatedProduct = {
			productName: reqBody.productName,
			description: reqBody.description,
			price: reqBody.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false
			} else {
				return "Successfully Updated!"
			}
		})	
}

// Archiving Product
module.exports.archiveProduct = (reqParams) => {
	// console.log(reqParams);
	let archivePro = {
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, archivePro).then((product, error) => {
		if(error){
			return false
		} else {
			return `Archive Successfull!`
		}
	})
}