const jwt = require("jsonwebtoken");
// Secret
const secret = "WeGotItAllForYOU!";

// JSON Web Tokens
// Token Creation
module.exports.createAccessToken = (user) => {
	 // console.log(user)

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {})
}

// Token verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	// Token received and is not undefined
	if(typeof token !== "undefined") {
		// console.log(token);
		token = token.slice(7, token.length);

		// Validating the token
		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not valid
			if(err) {
				return res.send({auth: "failed"});
			// JWT is valid
			} else { 
				next();
			} 
		})
	// Token does not exist
	} else {
		return res.send({auth: "Does not exist!"});
	}
}

// Token Decryption
module.exports.decode = (token) => {
	// console.log(token);
	// Token received and is not undefined
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return "Error Tok Dec!";
	}
}