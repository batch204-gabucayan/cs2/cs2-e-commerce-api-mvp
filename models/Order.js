const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String
	},
	products: [
		{
			productId: {
				type: String,
				require: [true, ""]
			},
			productName: {
				type: String
			},
			price: {
				type: Number
			},
			quantity: {
				type: Number,
				required: [true, ""]
			},
			subTotalAmount: Number
		}
	],
	orderedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);