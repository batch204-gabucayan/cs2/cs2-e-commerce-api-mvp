const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "This is required!"]
	},
	lastName: {
		type: String,
		required: [true, "This is required!"]
	},
	dateOfBirth: {
		type: Date,
		required: [true, "This is required!"]
	},
	address: {
		type: String,
		required: [true, "This is required!"]
	},
	email: {
		type: String,
		required: [true, "This is required!"]
	},
	password: {
		type: String,
		required: [true, "This is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: Number,
		required: [true, "This is required!"]
	},
	orders: [
		{
			orderId: {
				type: String
			},
			products: [
				{
					productId: {
						type: String
					},
					productName: {
						type: String
					},
					price: {
						type: Number
					},
					quantity: {
						type: Number
					},
					subTotalAmount: Number
				}
			],
			totalAmount: {
				type: Number
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);